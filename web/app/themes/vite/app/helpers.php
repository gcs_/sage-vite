<?php

namespace App;

/**
 * Build assets hmr uri
 *
 * @param string $asset
 * @return string
 */
function hmr_assets(string $asset): string
{
    return "http://localhost:3000/{$asset}";
}
